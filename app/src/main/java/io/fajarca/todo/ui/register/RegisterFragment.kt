package io.fajarca.todo.ui.register


import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.fajarca.todo.R
import io.fajarca.todo.base.BaseFragment
import io.fajarca.todo.databinding.FragmentRegisterBinding
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.btnRegister
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_register
    override fun getViewModelClass() = RegisterViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        etName.addTextChangedListener(textWatcher)
        etMobilePhone.addTextChangedListener(textWatcher)
        etPIN.addTextChangedListener(textWatcher)
        etPINRepeat.addTextChangedListener(textWatcher)

        btnRegister.setOnClickListener {
            val name = etName.text.toString().trim()
            val phone = etMobilePhone.text.toString().trim()
            val pin = etPIN.text.toString().trim()
            val pinRepeat = etPINRepeat.text.toString().trim()

            vm.validateInput(name, phone, pin, pinRepeat) }
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val name = etName.text.toString().trim().isNotEmpty()
            val phone = etMobilePhone.text.toString().trim().isNotEmpty()
            val pin = etPIN.text.toString().trim().isNotEmpty()
            val pinRepeat = etPINRepeat.text.toString().trim().isNotEmpty()

            val condition = name && phone && pin && pinRepeat
            setDisplayBtnLogin(condition)
        }

    }

    fun setDisplayBtnLogin(condition:Boolean){
        btnRegister.isEnabled = condition

        if (condition){
            btnLogin.setBackgroundColor(Color.GRAY)
        } else{
            btnLogin.setBackgroundColor(0xFF9800)
        }
    }


}
