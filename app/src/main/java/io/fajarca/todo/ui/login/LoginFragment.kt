package io.fajarca.todo.ui.login


import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation

import io.fajarca.todo.R
import io.fajarca.todo.base.BaseFragment
import io.fajarca.todo.data.remote.response.LoginResponse
import io.fajarca.todo.databinding.FragmentLoginBinding
import io.fajarca.todo.vo.Result
import io.fajarca.todo.vo.ValidationResult
import kotlinx.android.synthetic.main.fragment_login.*
import timber.log.Timber


class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(),
    View.OnClickListener {

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.btnRegister -> { navController.navigate(R.id.action_loginFragment_to_registerFragment) }
        }
    }

    override fun getLayoutResourceId() = R.layout.fragment_login
    override fun getViewModelClass() = LoginViewModel::class.java

    private lateinit var navController : NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        btnLogin.setOnClickListener {
            val phoneNumber = etPhoneNumber.text.toString().trim()
            val pinNumber = etPINNumber.text.toString().trim()
            vm.validateInput(phoneNumber, pinNumber)
        }

        btnRegister.setOnClickListener (this)

        vm.userExist.observe(this, Observer{subscribeUserExist(it)})
        vm.login.observe(this, Observer { subscribeLoginValidation(it) })

        etPhoneNumber.addTextChangedListener(textWatcher)
        etPINNumber.addTextChangedListener(textWatcher)

        navController = Navigation.findNavController(view)

        if (vm.loginCheck() == true) {
            navController.navigate(R.id.action_loginFragment_to_listVoucherFragment)
        }

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val phoneNumber = etPhoneNumber.text.toString().trim().isNotEmpty()
            val pinNumber = etPINNumber.text.toString().trim().isNotEmpty()

            val condition = phoneNumber && pinNumber
            setDisplayBtnLogin(condition)
        }

    }

    fun setDisplayBtnLogin(condition:Boolean){
        btnLogin.isEnabled = condition

        if (!condition){
            btnLogin.setBackgroundColor(Color.GRAY)
        } else{
            btnLogin.setBackgroundColor(0xFF9800)
        }
    }

    private fun subscribeLoginValidation(result: ValidationResult?) {
        when (result) {
            is ValidationResult.VALID -> {
                longSnackbar("Valid")
            }
            is ValidationResult.INVALID -> {
                longSnackbar(result.errorMessage)
            }
        }
    }

    private fun subscribeUserExist(it: Result<LoginResponse>) {
        when(it.status) {
            Result.Status.LOADING -> {
                longSnackbar("Proses login ...")
                Timber.v("Status : Loading")
            }
            Result.Status.SUCCESS -> {
                longSnackbar("Login berhasil")

                val phoneNumber = etPhoneNumber.text.toString().trim()
                val pinNumber = etPINNumber.text.toString().trim()
                vm.saveLogin(phoneNumber, pinNumber.toInt())

                //## move to list fragment
                navController.navigate(R.id.action_loginFragment_to_listVoucherFragment)

                Timber.v("Status : Success")
            }
            Result.Status.ERROR -> {
                longSnackbar(it.message!!)
                Timber.v("Status : Error" )
            }
        }
    }


}
