package io.fajarca.todo.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.todo.data.remote.response.LoginResponse
import io.fajarca.todo.data.repository.LoginRepository
import io.fajarca.todo.util.CoroutinesDispatcherProvider
import io.fajarca.todo.vo.Result
import io.fajarca.todo.vo.ValidationResult
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor
    (private val repository: LoginRepository,
     private val dispatcher: CoroutinesDispatcherProvider
)
    : ViewModel()  {

    var result = false

    private val _login = MutableLiveData<ValidationResult>()
    val login: LiveData<ValidationResult>
        get() = _login

    private val _userExist = MutableLiveData<Result<LoginResponse>>()
    val userExist: LiveData<Result<LoginResponse>>
        get() = _userExist


    fun validateInput(mobilePhone:String, PINNumber:String){

        if(mobilePhone.length !in 11..12){
            _login.value = ValidationResult.INVALID("Nomor telepon invalid")
        } else if(PINNumber.length < 6){
            _login.value = ValidationResult.INVALID("Nomor PIN harus 6 digit")
        }
        else{
            isUserExist(mobilePhone, PINNumber.toInt())
        }

    }

    fun isUserExist(mobilePhone:String, PINNumber:Int) {
        _userExist.postValue(Result.loading(null))
        viewModelScope.launch(dispatcher.io) {
            val loginResult = repository.getExistedUser(mobilePhone, PINNumber)
            _userExist.postValue(loginResult)
        }

    }

    fun saveLogin(mobileNumber:String, pin:Int) = repository.saveSession(mobileNumber, pin)

    fun loginCheck() = repository.conditionLoginCheck()
}