package io.fajarca.todo.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.todo.data.remote.HomeRemoteDataSource
import io.fajarca.todo.data.remote.response.DetailVoucherResponse
import io.fajarca.todo.data.repository.DetailVoucherRepository
import io.fajarca.todo.data.repository.HomeRepository
import io.fajarca.todo.util.CoroutinesDispatcherProvider
import kotlinx.coroutines.launch
import javax.inject.Inject
import io.fajarca.todo.vo.Result
import java.text.SimpleDateFormat
import java.util.*

class DetailViewModel @Inject constructor(
    private val repository: DetailVoucherRepository,
    private val dispatcher: CoroutinesDispatcherProvider
): ViewModel()  {

    private val _detailVoucher = MutableLiveData<Result<DetailVoucherResponse>>()
    val detailVoucher: LiveData<Result<DetailVoucherResponse>>
        get() = _detailVoucher

    fun getAllDetailVoucher(id: Int) {
        _detailVoucher.postValue(Result.loading(null))
        viewModelScope.launch(dispatcher.io) {
            _detailVoucher.postValue(repository.getAllDetailVoucher(id))
        }
    }

    fun dateFormat(expireDate: String): String{
        val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val dateParse = formatter.parse(expireDate)
        val day = SimpleDateFormat("dd").format(dateParse)
        val month = SimpleDateFormat("MMMM").format(dateParse)
        val year = SimpleDateFormat("yyyy").format(dateParse)
        val dateResult = "$day $month $year"
        return dateResult
    }

}