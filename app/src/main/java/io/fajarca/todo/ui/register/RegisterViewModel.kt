package io.fajarca.todo.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.fajarca.todo.data.repository.RegisterRepository
import io.fajarca.todo.util.CoroutinesDispatcherProvider
import io.fajarca.todo.vo.ValidationResult
import javax.inject.Inject

class RegisterViewModel @Inject constructor
    (private val repository: RegisterRepository,
     private val dispatcher: CoroutinesDispatcherProvider
): ViewModel()  {

    private val _register = MutableLiveData<ValidationResult>()
    val register: LiveData<ValidationResult>
        get() = _register

    fun validateInput(name:String, phone:String, pin:String, pinRepeat:String){

        if(name.isNullOrEmpty()){
            _register.value = ValidationResult.INVALID("Nama belum diisi")
        } else if (phone.isNullOrEmpty()){
            _register.value = ValidationResult.INVALID("Nomor telepon belum diisi")

        } else if (pin.isNullOrEmpty()){
            _register.value = ValidationResult.INVALID("PIN belum diisi")

        } else if (pinRepeat.isNullOrEmpty()){
            _register.value = ValidationResult.INVALID("Ulangi PIN")

        } else if(phone.length !in 11..12){
            _register.value = ValidationResult.INVALID("Nomor telepon invalid")
        } else if(pinRepeat.length < 6){
            _register.value = ValidationResult.INVALID("Nomor PIN harus 6 digit")
        }
        else{
            //## send data register
        }

    }

    fun isNameValid(name: String): Boolean {
        if (!name.isNullOrEmpty()) {
            return true
        }
        return false
    }

    fun isPhoneValid(phone: String): Boolean {
        if (!phone.isNullOrEmpty()) {
            return true
        }
        return false
    }
}