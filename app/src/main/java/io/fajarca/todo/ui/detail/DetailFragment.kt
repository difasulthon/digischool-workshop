package io.fajarca.todo.ui.detail


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import io.fajarca.todo.R
import io.fajarca.todo.base.BaseFragment
import io.fajarca.todo.data.remote.response.Data
import io.fajarca.todo.data.remote.response.DetailVoucherResponse
import io.fajarca.todo.databinding.FragmentDetailBinding
import io.fajarca.todo.vo.Result
import kotlinx.android.synthetic.main.fragment_detail.*
import timber.log.Timber


class DetailFragment : BaseFragment<FragmentDetailBinding, DetailViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_detail
    override fun getViewModelClass() = DetailViewModel::class.java
    var idVoucher: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idVoucher = arguments!!.getInt("id")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.getAllDetailVoucher(idVoucher!!)
        vm.detailVoucher.observe(this, Observer { subscribeDetailVoucher(it) })

        Toast.makeText(this.activity, "id dari list : $idVoucher", Toast.LENGTH_SHORT).show()

        sbUse.setOnStateChangeListener {active ->
            if (active) {
                Toast.makeText(this.activity, "Voucher Berhasil Dipakai", Toast.LENGTH_SHORT).show()
            }
            else {

            }
        }
    }

    private fun subscribeDetailVoucher(it: Result<DetailVoucherResponse>) {
        when(it.status) {
            Result.Status.LOADING -> {
                Timber.v("Status : Loading")
                binding.tvLoading.isVisible = true
            }
            Result.Status.SUCCESS -> {
                Timber.v("Status : Success")
                binding.tvLoading.isVisible = false
                displayVoucherDetails(it.data?.data)
            }
            Result.Status.ERROR -> {
                Timber.v("Status : Error" )
            }
        }
    }

    private fun displayVoucherDetails(data: Data?) {
        data?.let {
            binding.detail = data
            binding.tvDetailDescription.text ="${data.description}"
            binding.tvVoucherName.text = "${data.name}"
            binding.tvDetailTerm.text = "${data.snk}"
            val expireDate = "${data.date}"
            binding.tvDate.text = vm.dateFormat(expireDate)
        }
    }

}
