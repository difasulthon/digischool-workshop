package io.fajarca.todo.data.remote

import io.fajarca.todo.base.BaseRemoteDataSource
import javax.inject.Inject

class RegisterRemoteDataSource @Inject constructor(private val api: ApiService) : BaseRemoteDataSource() {
}