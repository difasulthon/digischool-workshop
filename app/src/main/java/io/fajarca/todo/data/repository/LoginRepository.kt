package io.fajarca.todo.data.repository

import android.content.SharedPreferences
import android.content.pm.LauncherApps
import io.fajarca.todo.data.remote.LoginRemoteDataSource
import io.fajarca.todo.util.KEY_LOGIN_FLAG
import io.fajarca.todo.util.KEY_PHONE_NUMBER
import io.fajarca.todo.util.KEY_PIN_NUMBER
import javax.inject.Inject

class LoginRepository @Inject constructor(private val remoteDataSource: LoginRemoteDataSource,
                                          private val editor: SharedPreferences.Editor,
                                          private val retrieveSharedPreferences: SharedPreferences) {

    suspend fun getExistedUser(mobileNumber:String, pin:Int) = remoteDataSource.existedUser(mobileNumber, pin)

    fun saveSession(mobileNumber: String, pin: Int) {
        editor.putString(KEY_PHONE_NUMBER,mobileNumber)
        editor.putString(KEY_PIN_NUMBER, pin.toString())
        editor.putBoolean(KEY_LOGIN_FLAG,true)

        editor.apply()
    }

    fun conditionLoginCheck(): Boolean{
        var loginCheck = retrieveSharedPreferences.getBoolean(KEY_LOGIN_FLAG, false)
        return loginCheck
    }
}