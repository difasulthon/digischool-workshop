package io.fajarca.todo.data.remote

import io.fajarca.todo.base.BaseRemoteDataSource
import javax.inject.Inject

class ListVoucherRemoteDataSource @Inject constructor(private val api: ApiService) : BaseRemoteDataSource() {

    suspend fun listVoucher() = getApiResult {  api.listVoucher() }

}