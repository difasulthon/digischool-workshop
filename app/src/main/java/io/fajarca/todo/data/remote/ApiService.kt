package io.fajarca.todo.data.remote


import io.fajarca.todo.data.remote.response.DetailVoucherResponse
import io.fajarca.todo.data.remote.response.ListVoucherResponse
import io.fajarca.todo.data.remote.response.LoginResponse
import io.fajarca.todo.data.remote.response.NowPlayingResponse
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    @GET("now_playing")
    suspend fun nowPlaying(
        @Query("language") language: String = "en-US",
        @Query("page") page: Int = 1
    ): Response<NowPlayingResponse>

    @Headers ("Content-Type: application/json")
    @GET("v1/voucher/{id}")
    suspend fun detailVoucher(
        @Path ("id") id: Int
    ): Response<DetailVoucherResponse>

    @Headers("Content-Type: application/json")
    @GET("v1/voucher")
    suspend fun listVoucher(
        @Query("page") page: Int = 1,
        @Query("size") size : Int = 10
    ): Response<ListVoucherResponse>

    @POST("v1/customers/auth/login")
    suspend fun getExistedUser(
        @Query("mobileNumber") mobileNumber:String,
        @Query("pin") pin:Int
    ): Response<LoginResponse>

}