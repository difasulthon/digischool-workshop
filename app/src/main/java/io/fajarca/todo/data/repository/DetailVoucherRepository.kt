package io.fajarca.todo.data.repository

import io.fajarca.todo.data.remote.DetailVoucherRemoteDataSource
import javax.inject.Inject

class DetailVoucherRepository @Inject constructor(private val remoteDataSource: DetailVoucherRemoteDataSource) {

    suspend fun getAllDetailVoucher(id: Int) = remoteDataSource.detailVoucher(id)
}