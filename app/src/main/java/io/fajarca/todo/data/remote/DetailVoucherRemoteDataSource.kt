package io.fajarca.todo.data.remote

import io.fajarca.todo.base.BaseRemoteDataSource
import javax.inject.Inject

class DetailVoucherRemoteDataSource @Inject constructor(private val api: ApiService) : BaseRemoteDataSource() {

    suspend fun detailVoucher(id: Int) = getApiResult { api.detailVoucher(id) }
}