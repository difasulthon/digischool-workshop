package io.fajarca.todo.data.remote.response

import com.google.gson.annotations.SerializedName

data class DetailVoucherResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)

data class Data(
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("date")
    val date: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("issuer")
    val issuer: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("promoCode")
    val promoCode: String,
    @SerializedName("snk")
    val snk: String,
    @SerializedName("voucherId")
    val voucherId: String
)