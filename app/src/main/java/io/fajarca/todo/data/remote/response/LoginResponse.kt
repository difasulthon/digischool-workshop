package io.fajarca.todo.data.remote.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("data")
    val data: TokenLoginResponse,
    @SerializedName("message")
    val message: String,
    @SerializedName("code")
    val code: Int
)

data class TokenLoginResponse(
    @SerializedName("token")
    val token: String
)