package io.fajarca.todo.data.remote

import io.fajarca.todo.base.BaseRemoteDataSource
import javax.inject.Inject

class LoginRemoteDataSource @Inject constructor(private val api: ApiService) : BaseRemoteDataSource()  {

    suspend fun existedUser(mobileNumber:String, pin:Int) = getApiResult {  api.getExistedUser(mobileNumber, pin) }
}