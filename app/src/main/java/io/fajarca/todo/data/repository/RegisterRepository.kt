package io.fajarca.todo.data.repository

import io.fajarca.todo.data.remote.LoginRemoteDataSource
import javax.inject.Inject

class RegisterRepository @Inject constructor(private val remoteDataSource: LoginRemoteDataSource) {

    suspend fun getExistedUser(mobileNumber:String, pin:Int) = remoteDataSource.existedUser(mobileNumber, pin)
}