package io.fajarca.todo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.fajarca.todo.ui.detail.DetailViewModel
import io.fajarca.todo.ui.home.HomeViewModel
import io.fajarca.todo.ui.list.ListVoucherViewModel
import io.fajarca.todo.ui.login.LoginViewModel
import io.fajarca.todo.ui.register.RegisterViewModel
import io.fajarca.todo.util.ViewModelFactory
import io.fajarca.todo.util.ViewModelKey

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun providesHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun providesDetailViewModel(viewModel: DetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListVoucherViewModel::class)
    internal abstract fun providesListVoucherViewModel(viewModel: ListVoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun providesLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun providesRegisterViewModel(viewModel: RegisterViewModel): ViewModel


}