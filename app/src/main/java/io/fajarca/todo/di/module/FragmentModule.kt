package io.fajarca.todo.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.fajarca.todo.ui.detail.DetailFragment
import io.fajarca.todo.ui.home.HomeFragment
import io.fajarca.todo.ui.list.ListVoucherFragment
import io.fajarca.todo.ui.login.LoginFragment
import io.fajarca.todo.ui.register.RegisterFragment


@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributesHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailFragment(): DetailFragment

    @ContributesAndroidInjector
    abstract fun contributesListVoucherFragment(): ListVoucherFragment

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributesRegisterFragment(): RegisterFragment

}